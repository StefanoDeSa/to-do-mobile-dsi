import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import React from "react";
import TopBar from "./components/TopBar";
import ScanQrCode from "./components/ScanQrCode";
import Login from "./components/Login";

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Home" component={TopBar} />
        <Stack.Screen name="ScanQrCode" component={ScanQrCode} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
