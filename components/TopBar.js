// No componente TopBar.js, onde você lida com o botão de escaneamento

import React from 'react';
import { View } from 'react-native';
import { Appbar} from 'react-native-paper';
import Form from './Form';
import TaskList from './TaskList';

const TopBar = ({ navigation }) => {
  return (
    <View>
      <Appbar.Header>
        <Appbar.Content title="To do list" />
        <Appbar.Action icon="qrcode" onPress={() => navigation.navigate('ScanQrCode')} />
      </Appbar.Header>
      <Form />
      <TaskList />
    </View>
  );
};

export default TopBar;