import { useState, useEffect } from "react";
import { View, Text, StyleSheet } from "react-native";
import { Checkbox, Button, TextInput } from "react-native-paper";
import { listAllTasks, deleteTask, updateTask, updateStatus } from "../services/TaskService";

export default function TaskList() {
  const [tasks, setTasks] = useState([]);
  const [texto, setTexto] = useState("");
  const [taskId, setTaskId] = useState(null);

  useEffect(() => {
    async function fetchTasks() {
      const tasksFromService = await listAllTasks();
      setTasks(tasksFromService);
    }

    fetchTasks();
  }, [tasks]);

  const handleEdit = (task) => {
    setTaskId(task.id);
    setTexto(task.nome);
  };

  const handleUpdate = async (id) => {
    await updateTask(id, texto);
    setTaskId(null);
    setTexto("");
  };

  const handleConcluir = async (id, status) => {
    await updateStatus(id, status);
  };

  const handleDelete = (id) => {
    deleteTask(id);
  };

  return (
    <View style={styles.container}>
      {tasks.map((task, index) => (
        <View key={index} style={styles.taskContainer}>
          <View style={styles.taskItem}>
            <Checkbox.Item
              status={task.status ? "checked" : "unchecked"}
              onPress={() => {
                handleConcluir(task.id, task.status);
              }}
            />
            <View style={styles.textContainer}>
              {taskId === task.id ? (
                <TextInput
                  value={texto}
                  onChangeText={setTexto}
                  onBlur={() => handleUpdate(task.id)}
                  mode="outlined"
                />
              ) : (
                <Text style={task.status ? styles.textoConcluido : styles.taskText}>{task.nome}</Text>
              )}
            </View>
          </View>
          <View style={styles.buttonContainer}>
            <Button
              icon="pencil"
              style={[styles.button, styles.iconButton]}
              onPress={() => handleEdit(task)}
            />
            <Button
              icon="delete"
              style={[styles.button, styles.iconButton]}
              onPress={() => handleDelete(task.id)}
            />
          </View>
        </View>
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    paddingHorizontal: 10,
    marginBottom: 10,
  },
  taskContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 10,
    width: "100%",
  },
  taskItem: {
    flexDirection: "row",
    alignItems: "center",
    flex: 1,
  },
  textContainer: {
    maxWidth: "75%",
  },
  taskText: {
    marginLeft: 10,
  },
  buttonContainer: {
    flexDirection: "row",
  },
  iconButton: {
    marginTop: 5,
  },
  textoConcluido: {
    textDecorationLine: "line-through",
    color: 'gray'
  }
});
