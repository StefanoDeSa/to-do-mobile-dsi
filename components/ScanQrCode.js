import React, { useState, useEffect } from "react";
import { Text, View, StyleSheet } from "react-native";
import { CameraView, Camera } from "expo-camera/next";
import { createTask } from "../services/TaskService";
import { Modal, Portal, Button, PaperProvider, List } from "react-native-paper";

export default function ScanQrCode({ navigation }) {
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [qrData, setQrData] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const getCameraPermissions = async () => {
      const { status } = await Camera.requestCameraPermissionsAsync();
      setHasPermission(status === "granted");
    };

    getCameraPermissions();
  }, []);

  const handleBarCodeScanned = ({ data }) => {
    setScanned(true);
    const array = data.split(";").filter((item) => item.trim() !== "");

    if (array.length > 0) {
      setShowModal(true);
      setQrData(array);
    } else {
      console.log("Informe um QRCODE válido");
    }
  };

  const handleConfirm = () => {
    setLoading(true);
    qrData.map((item) => createTask(item));
    setLoading(false);
    setShowModal(false);
    alert("Qr code lido e as tarefas foram criadas");
    setTimeout(() => {
      navigation.replace('Home')
    }, 3000);
  };

  const handleCancel = () => {
    setShowModal(false);
  };

  if (hasPermission === null) {
    return <Text>Solicitando permissão da camera</Text>;
  }
  if (hasPermission === false) {
    return <Text>Permissão da camera negada</Text>;
  }

  return (
    <View style={styles.container}>
      <CameraView
        onBarcodeScanned={scanned ? undefined : handleBarCodeScanned}
        style={StyleSheet.absoluteFillObject}
      />
      {scanned && !showModal && (
        <Button mode="elevated" onPress={() => setScanned(false)}>
          Clique para escanear novamente
        </Button>
      )}

      {showModal && (
        <PaperProvider>
          <Portal>
            <Modal
              visible={showModal}
              onDismiss={handleCancel}
              contentContainerStyle={styles.modal}
            >
              <Text>Deseja adicionar as tarefas:</Text>
              {qrData.map((item, index) => (
                <List.Item key={index} title={item} />
              ))}
              <View style={styles.buttonContainer}>
                <Button mode="contained" onPress={handleCancel}>
                  Sair
                </Button>
                <Button mode="contained" onPress={handleConfirm}>
                  Cadastrar
                </Button>
              </View>
            </Modal>
          </Portal>
        </PaperProvider>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    flexDirection: "column",
    justifyContent: "center",
  },
  modal: {
    backgroundColor: "white",
    padding: 20,
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 20,
  },
});
