import { View, Text, StyleSheet } from "react-native";
import { useState, useEffect } from "react";
import { TextInput, Button } from "react-native-paper";
import { create, loginUser } from "../services/AuthService";

export default function Login({ navigation }) {
  const [login, setLogin] = useState("");
  const [senha, setSenha] = useState("");
  const [loading, setLoading] = useState(false);

  const handleCadastrar = async () => {
    try {
      setLoading(true);
      const dados = { login: login, senha: senha };
      await create(dados);
      alert("Conta criada com sucesso");
      setLoading(false);
    } catch (error) {
      alert("Ocorreu um erro ao criar a conta: " + error.message);
      setLoading(false);
    }
  };

  const handleLogin = async () => {
    try {
      setLoading(true);
      const dados = { login: login, senha: senha };
      const userInfo = await loginUser(dados);
      if (userInfo) {
        alert(`O usuário ${userInfo.login} foi autenticado com sucesso`);

        navigation.navigate("Home");
      }
      setLoading(false);
    } catch (error) {
      alert("Ocorreu um erro ao logar na conta: " + error.message);
      setLoading(false);
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>To-Do Login</Text>
      <View style={styles.inputContainer}>
        <TextInput
          label="Login"
          value={login}
          onChangeText={(login) => setLogin(login)}
          mode="outlined"
        />
        <TextInput
          label="Senha"
          value={senha}
          onChangeText={(senha) => setSenha(senha)}
          mode="outlined"
        />
        <View style={styles.buttonContainer}>
          <Button
            style={styles.button}
            mode="contained"
            buttonColor="green"
            disabled={loading}
            onPress={() => handleLogin()}
          >
            Login
          </Button>
          <Button
            style={styles.button}
            mode="contained"
            buttonColor="blue"
            disabled={loading}
            onPress={() => handleCadastrar()}
          >
            Cadastrar
          </Button>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
  },
  inputContainer: {
    width: "80%",
  },
  buttonContainer: {
    justifyContent: "space-between",
    marginVertical: 20,
  },
  button: {
    marginVertical: 10,
  },
});
