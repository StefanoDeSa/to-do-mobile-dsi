import React, { useState, useEffect } from "react";
import { View, StyleSheet } from 'react-native';
import { TextInput, IconButton, Button } from 'react-native-paper';

import { AntDesign } from '@expo/vector-icons';
import { createTask } from "../services/TaskService";

export default function Form() {
    const [text, setText] = React.useState("");
    const [loading, setLoading] = useState(false);

    const handleConfirm = async () => {
        setLoading(true);
        await createTask(text);
        setText('');
        setLoading(false);
        console.log('Tarefa adicionada com sucesso')

    };

    return (
        <View style={styles.container}>
            <TextInput
                label="Escreva uma tarefa"
                value={text}
                onChangeText={text => setText(text)}
                style={styles.input}
                backgroundColor="#DCDCDC"
            />
            <IconButton
                icon={() => <AntDesign name="plus" size={24} color="white" />}
                onPress={handleConfirm}
                style={styles.iconButton}
            />
        </View>


    );
};
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        maxWidth: '94%',
        left: 17,
        marginTop: 25,
    },
    input: {
        flex: 1,
        marginBottom: 10,

    },
    button: {
        width: '40%',
        alignSelf: "center",
    },
    iconButton: {
        backgroundColor: '#1E90FF',
    }

});

