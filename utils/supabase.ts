import 'react-native-url-polyfill/auto'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { createClient } from '@supabase/supabase-js'


const supabaseUrl = 'https://lueigstlfvdgtvcwgnho.supabase.co'
const supabaseAnonKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Imx1ZWlnc3RsZnZkZ3R2Y3dnbmhvIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTI4NjMwMjMsImV4cCI6MjAyODQzOTAyM30.nXgHkP_rR1FNHeGP3rmwXwHL3TzNBP8Ewq8PZq9N_pk'


export const supabase = createClient(supabaseUrl, supabaseAnonKey, {
  auth: {
    storage: AsyncStorage,
    autoRefreshToken: true,
    persistSession: true,
    detectSessionInUrl: false,
  },
})