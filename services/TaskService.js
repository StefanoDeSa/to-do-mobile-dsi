import { supabase } from "../utils/supabase";

export async function createTask(tarefa) {
  try {
    const task = await supabase.from("tarefa").insert({ nome: tarefa });
    console.log(`Tarefa ${tarefa} criada com sucesso`);
  } catch (error) {
    console.error("Erro ao criar tarefa:", error.message);
    return null;
  }
}

export async function listAllTasks() {
  try {
    const { data, error } = await supabase
      .from("tarefa")
      .select()
      .order("status", { ascending: true })
      .order("data_criacao", { ascending: false });
    if (error) {
      throw new Error(error.message);
    }
    return data;
  } catch (error) {
    console.error("Erro ao buscar tarefas:", error.message);
    return [];
  }
}

export async function deleteTask(id) {
  try {
    const task = await supabase.from("tarefa").delete().eq("id", id);
    console.log(`Tarefa com id ${id} deletada com sucesso`);
  } catch (error) {
    console.error("Erro ao deletar a tarefa:", error.message);
    return null;
  }
}

export async function updateTask(id, texto) {
  try {
    const task = await supabase.from("tarefa").update({ nome: texto }).eq("id", id);
    console.log(`Tarefa com id ${id} atualizada com sucesso`);
  } catch (error) {
    console.error("Erro ao atualizar a tarefa:", error.message);
    return null;
  }
}

export async function updateStatus(id, status) {
  try {
    const task = await supabase.from("tarefa").update({ status: !status }).eq("id", id);
    console.log(`Tarefa com id ${id} teve o status alterado para ${!status} sucesso`);
  } catch (error) {
    console.error("Erro ao concluir a tarefa:", error.message);
    return null;
  }
}
