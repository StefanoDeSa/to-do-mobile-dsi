import { supabase } from "../utils/supabase";

export async function create(dados) {
  try {
    const { data, error } = await supabase.from('usuario').insert({
      login: dados.login,
      senha: dados.senha,
    });

    if (error) {
      throw error;
    }

    return data;
  } catch (error) {
    throw error;
  }
}

export async function loginUser(dados){
    try {
      const { data, error } = await supabase.from('usuario').select().eq("login", dados.login, "senha", dados.senha);
    
        if (error) {
          throw error;
        }

        if (data.length === 0) {
          throw new Error('Usuário não encontrado ou senha incorreta');
        }
    
        return data[0];
      } catch (error) {
        throw error;
      }
}
